package uk.co.rhysbotfield.magentocertquiz.helpers;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by rhys4 on 12/10/2017.
 */

public abstract class Data {

    /**
     * Choose n amount of random distinct numbers from 0 to max
     *
     * @param max The total range to be shuffled
     * @param n   The amount of numbers to randomly choose from 0 to max
     * @return An ArrayList of n amount of random distinct numbers between 0 and max.
     * @throws IllegalArgumentException If n is larger than the max number
     */
    public static List getDistinctNumbers(int max, int n) {
        if (n > max)
            throw new IllegalArgumentException("Amount of numbers to choose is larger than the collection");
        List numbers = new ArrayList();
        for (int i = 0; i < max; i++)
            numbers.add(i);
        Collections.shuffle(numbers);
        numbers = numbers.subList(0, n);
        return numbers;
    }

    /**
     * Parse the contents of an exam questions XML file into ArrayList<ArrayLists<String>>.
     * The inner ArrayList<String> is a question, answer, and whether it has multiple answers.
     * In the case that multiAnswer is true, the answer should be a comma-delimited String.
     *
     * @param xml XmlPullParser Resource for exam questions.
     * @return ArrayList of questions and answers.
     */
    public static ArrayList<ArrayList<String>> getQAndAs(XmlPullParser xml) {
        ArrayList<ArrayList<String>> qandas = new ArrayList<>();

        try {
            int eventType = xml.getEventType();
            boolean hasQuestion = false, hasAnswer = false, hasMultiAnswer = false;
            String question = null;
            String answer = null;
            String multiAnswer = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (xml.getName().equals("question_string")) {
                        question = xml.nextText();
                        hasQuestion = true;
                    }
                    if (xml.getName().equals("answer")) {
                        answer = xml.nextText();
                        hasAnswer = true;
                    }
                    if (xml.getName().equals("multianswer")) {
                        multiAnswer = xml.nextText();
                        if (!multiAnswer.equals("false") && !multiAnswer.equals("true"))
                            throw new IllegalArgumentException("multiAnswer in XML question is not true or false. XML Data: " + multiAnswer);
                        hasMultiAnswer = true;
                    }
                }
                if (eventType == XmlPullParser.END_TAG && xml.getName().equals("question")) {
                    if (!hasQuestion || !hasAnswer || !hasMultiAnswer) {
                        throw new NullPointerException("question node ended without all data filled.");
                    } else {
                        ArrayList<String> qAndA = new ArrayList<>();
                        qAndA.add(question);
                        qAndA.add(answer);
                        qAndA.add(multiAnswer);
                        qandas.add(qAndA);
                        hasAnswer = false;
                        hasMultiAnswer = false;
                        hasQuestion = false;
                    }
                }
                eventType = xml.next();
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return qandas;
    }
}
