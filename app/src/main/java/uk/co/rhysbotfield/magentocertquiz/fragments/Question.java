package uk.co.rhysbotfield.magentocertquiz.fragments;

/**
 * Created by rhys4 on 15/10/2017.
 */

public interface Question {

    /**
     * Check if the UI has the correct checkboxes selected
     *
     * @return boolean true|false if correct answers are selected
     */
    boolean isCorrect();

    /**
     * Check that any answer has been selected.
     * @return true|false if any answer is chosen.
     */
    boolean isSelected();

    /**
     * Disable clickability on the answers
     */
    void disableAnswers();

    /**
     * Display the correct & wrong answers to the user in the UI
     */
    void highlightAnswer();

    /**
     * Setter for private field question
     *
     * @param question String input for question
     */
    void setQuestion(String question);

    /**
     * Setter for private field answer
     *
     * @param answer String comma-delimited list of the correct answers. Will only accept values A, B, C, and D
     */
    void setAnswer(String answer);

    /**
     * Getter for private field question
     *
     * @return String question to display in TextView
     */
    String getQuestion();

    /**
     * Getter for private field answer
     *
     * @return String answer to question
     */
    String getAnswer();

}
