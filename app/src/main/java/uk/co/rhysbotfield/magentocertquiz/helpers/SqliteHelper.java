package uk.co.rhysbotfield.magentocertquiz.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by rhys4 on 16/10/2017.
 */

public class SqliteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Results.db";
    private static final String TABLE_NAME = "results";
    private static final String COLUMN_NAME_SCORE = "score";
    private static final String COLUMN_NAME_MAXQUESTIONS = "max_questions";

    public SqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Called upon database creation to create the database tables
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME +
                " ( " + COLUMN_NAME_MAXQUESTIONS + " INT NOT NULL , "
                + COLUMN_NAME_SCORE + " INT NOT NULL )");
    }

    /**
     * Called upon database upgrade
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    /**
     * Add the score at the end of the quiz
     * @param maxQuestions int number of questions in the quiz
     * @param score int score in the quiz
     */
    public void addScore(int maxQuestions, int score) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_MAXQUESTIONS, maxQuestions);
        values.put(COLUMN_NAME_SCORE, score);

        db.insert(TABLE_NAME, null, values);
        db.close();

    }

    /**
     * Getter for limited number of scores from the previous results. Gets the last n results.
     * @param amount
     * @return ArrayList<ArrayList<Integer>> list of scores.
     */
    public ArrayList<ArrayList<Integer>> getScores(int amount ) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ArrayList<Integer>> scores = new ArrayList<>();

        String selectQuery = "SELECT rowid,* FROM `" + TABLE_NAME + "` ORDER BY `rowid` DESC LIMIT " + amount;
        Cursor cursor = db.rawQuery(selectQuery,null);
        while (cursor.moveToNext()) {
            ArrayList<Integer> score = new ArrayList<>();
            score.add(cursor.getInt(cursor.getColumnIndexOrThrow("rowid")));
            score.add(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_NAME_MAXQUESTIONS)));
            score.add(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_NAME_SCORE)));
            scores.add(score);
        }
        return scores;
    }

    /**
     * Get all the previous scores from the quiz.
     * @return  ArrayList<ArrayList<Integer>> list of scores.
     */
    public ArrayList<ArrayList<Integer>> getScores() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ArrayList<Integer>> scores = new ArrayList<>();

        String selectQuery = "SELECT rowid,* FROM `" + TABLE_NAME + "` ORDER BY `rowid` DESC ";
        Cursor cursor = db.rawQuery(selectQuery,null);
        while (cursor.moveToNext()) {
            ArrayList<Integer> score = new ArrayList<>();
            score.add(cursor.getInt(cursor.getColumnIndexOrThrow("rowid")));
            score.add(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_NAME_MAXQUESTIONS)));
            score.add(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_NAME_SCORE)));
            scores.add(score);
        }
        return scores;
    }
}
