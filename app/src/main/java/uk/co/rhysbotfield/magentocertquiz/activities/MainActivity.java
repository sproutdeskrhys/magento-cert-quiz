package uk.co.rhysbotfield.magentocertquiz.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

import uk.co.rhysbotfield.magentocertquiz.R;

public class MainActivity extends Activity {

    /**
     * Activity constructor called on activity creation to assign onClickListeners to the buttons.
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        Button button101 = findViewById(R.id.start_m70_101);
        Button button201 = findViewById(R.id.start_m70_201);
        Button button301 = findViewById(R.id.start_m70_301);
        Button buttonResults = findViewById(R.id.start_results);

        button101.setOnClickListener(gotoQuiz);
        button201.setOnClickListener(notAvailable);
        button301.setOnClickListener(notAvailable);
        buttonResults.setOnClickListener(gotoResults);
    }

    /**
     * Inflate the options menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Listener for selected menu item, which should load an alert dialog for the help option
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.help:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                dialogBuilder.setTitle(R.string.help_title)
                        .setMessage(R.string.help_content)
                        .setPositiveButton("Ok",null);
                AlertDialog dialog = dialogBuilder.create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Return a toast notification if the quiz type is not available
     */
    private View.OnClickListener notAvailable = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            Toast.makeText(getApplicationContext(), R.string.exam_unavailable, Toast.LENGTH_LONG).show();
        }
    };

    /**
     * Goto QuizActivity, quoting the quiz_code as an extra
     */
    private View.OnClickListener gotoQuiz = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(getApplicationContext(),QuizActivity.class);
            i.putExtra("quiz_code",v.getId());
            startActivity(i);
        }
    };

    /**
     * Goto ResultsActivity
     */
    private View.OnClickListener gotoResults = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(getApplicationContext(),ResultsActivity.class);
            startActivity(i);
        }
    };
}
