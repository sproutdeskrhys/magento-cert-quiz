package uk.co.rhysbotfield.magentocertquiz.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

import uk.co.rhysbotfield.magentocertquiz.R;
import uk.co.rhysbotfield.magentocertquiz.helpers.SqliteHelper;

public class ResultsActivity extends Activity {

    /**
     * ResultsActivity constructor that loads the scores from the database and passes them to the table loader.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        setTitle("Results");

        ArrayList<ArrayList<Integer>> scores = new SqliteHelper(this).getScores();

        TextView results_title = findViewById(R.id.results_title);
        if (scores.size() == 1)
            results_title.setText(R.string.results_title);
        else if (scores.size() > 1)
            results_title.setText("Your previous " + scores.size() + " results");
        else
            results_title.setText(R.string.no_results_title);

        loadTable(scores);
    }

    /**
     * Load the scores into the table and display them to the user in the UI
     * @param scores
     */
    private void loadTable(ArrayList<ArrayList<Integer>> scores){
        TableLayout table = findViewById(R.id.results_table);
        for (int i = 0 ; i < scores.size() ; i++){
            ArrayList<Integer> score = scores.get(i);

            TableRow row = new TableRow(this);
            row.setLayoutParams(findViewById(R.id.result_heading).getLayoutParams());

            TextView rowId = new TextView(this);
            rowId.setLayoutParams(findViewById(R.id.result_heading_id).getLayoutParams());
            rowId.setText(score.get(0).toString());
            rowId.setTextSize(20);

            TextView rowScore = new TextView(this);
            rowScore.setLayoutParams(findViewById(R.id.result_heading_score).getLayoutParams());
            rowScore.setText(score.get(2).toString() + " / " + score.get(1).toString());
            rowScore.setTextSize(20);

            row.addView(rowId);
            row.addView(rowScore);
            table.addView(row);
        }
    }
}
