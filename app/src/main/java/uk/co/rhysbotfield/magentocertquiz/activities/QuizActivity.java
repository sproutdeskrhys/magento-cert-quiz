package uk.co.rhysbotfield.magentocertquiz.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;

import uk.co.rhysbotfield.magentocertquiz.R;
import uk.co.rhysbotfield.magentocertquiz.fragments.*;
import uk.co.rhysbotfield.magentocertquiz.helpers.*;

public class QuizActivity extends FragmentActivity {

    private int currentQuestionNo = 1;
    private ArrayList<Fragment> fragments;
    private int currentScore, maxQuestions;

    /**
     * Activity constructor, which makes calls for loading the correct fragments and displaying the initial question.
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        String quiz_code = getQuizCode();
        if (null == quiz_code)
            throw new NullPointerException("quiz_code is null in onCreate after getQuizCode");

        setMaxQuestions(25);
        setTitle(quiz_code + " - Question " + getCurrentQuestionNo() + " - Score: " + getCurrentScore() + "/" + getMaxQuestions());

        setFragments(loadFragments());
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.fragment_container, getFragments().get(0)).commit();

        findViewById(R.id.submit).setOnClickListener(submitListener);
        findViewById(R.id.next).setOnClickListener(nextListener);
    }

    /**
     * Get the Quiz type via the intent of the activity.
     *
     * @return String|null Quiz Code. e.g. m70-101
     * @throws NullPointerException when the intent's quiz_code extra doesn't match.
     */
    private String getQuizCode() {
        if (getIntent().getExtras().get("quiz_code").equals(R.id.start_m70_101))
            return "M70-101";
        if (getIntent().getExtras().get("quiz_code").equals(R.id.start_m70_201))
            return "M70-201";
        if (getIntent().getExtras().get("quiz_code").equals(R.id.start_m70_301))
            return "M70-301";
        return null;
    }

    /**
     * @return ArrayList<Fragment> a list of fragments. The amount is equal to getMaxQuestions() and
     * they are randomly picked from the XML feed.
     */
    private ArrayList<Fragment> loadFragments() {
        XmlPullParser xml;
        switch (getQuizCode()) {
            case "M70-101":
                xml = getResources().getXml(R.xml.exam_m70_101);
                break;
            case "M70-201":
                xml = getResources().getXml(R.xml.exam_m70_201);
                break;
            case "M70-301":
                xml = getResources().getXml(R.xml.exam_m70_301);
                break;
            default:
                throw new NullPointerException("Unknown quiz_code in loadFragments");
        }

        ArrayList<ArrayList<String>> questionsData = Data.getQAndAs(xml);

        /* @throws IllegalArgumentException if noOfQuestions is larger than questions.size() */
        List questionNumbers = Data.getDistinctNumbers(questionsData.size(), getMaxQuestions());

        ArrayList<Fragment> fragments = new ArrayList<>();
        for (int i = 0; i < questionNumbers.size(); i++) {
            int questionNumber = (int) questionNumbers.get(i);
            ArrayList<String> questionData = questionsData.get(questionNumber);
            String question = questionData.get(0);
            String answer = questionData.get(1);
            boolean isMultiAnswer = Boolean.parseBoolean(questionData.get(2));

            Question questionFragment;
            if (isMultiAnswer) {
                questionFragment = new MultiAnswerQuestion();
                questionFragment.setQuestion(question);
                questionFragment.setAnswer(answer);
                fragments.add((Fragment) questionFragment);
            } else {
                questionFragment = new SingleAnswerQuestion();
                questionFragment.setQuestion(question);
                questionFragment.setAnswer(answer);
                fragments.add((Fragment) questionFragment);
            }

        }
        return fragments;
    }

    /**
     * onClickListener for answer submission. This handles outputting the result to the user and interacting with the UI upon submission
     * This button also saves the results if the last question is submitted.
     */
    private View.OnClickListener submitListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Question currentFragment = (Question) getFragments().get(getCurrentQuestionNo() - 1);
            if (!currentFragment.isSelected()) {
                Toast.makeText(getApplicationContext(), R.string.please_select, Toast.LENGTH_SHORT).show();
                return;
            }
            if (currentFragment.isCorrect()) {
                Toast.makeText(getApplicationContext(), R.string.correct, Toast.LENGTH_SHORT).show();
                setCurrentScore(getCurrentScore() + 1);
                setTitle(getQuizCode() + " - Question " + getCurrentQuestionNo() + " - Score: " + getCurrentScore() + "/" + getMaxQuestions());
            } else {
                Toast.makeText(getApplicationContext(), R.string.incorrect, Toast.LENGTH_SHORT).show();
            }

            findViewById(R.id.submit).setVisibility(View.INVISIBLE);
            TextView next = findViewById(R.id.next);
            if (getCurrentQuestionNo() == getMaxQuestions())
                next.setText(R.string.results);
            next.setVisibility(View.VISIBLE);
            currentFragment.disableAnswers();
            currentFragment.highlightAnswer();
            if (getCurrentQuestionNo() == getMaxQuestions()) {
                new SqliteHelper(getApplicationContext()).addScore(getMaxQuestions(),getCurrentScore());
            }
        }
    };

    /**
     * onClickListener for the "Next Question" button to load the next question fragment and change the activity title.
     * On the last fragment, the button changes to View Results to direct the user to the results page.
     */
    private View.OnClickListener nextListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getCurrentQuestionNo() == getMaxQuestions()) {
                Intent i = new Intent(getApplicationContext(), ResultsActivity.class);
                i.putExtra("score", getCurrentScore());
                i.putExtra("maxQuestions", getMaxQuestions());
                startActivity(i);
                finish();
                return;
            }
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, getFragments().get(getCurrentQuestionNo())).commit();
            setCurrentQuestionNo(getCurrentQuestionNo() + 1);
            setTitle(getQuizCode() + " - Question " + getCurrentQuestionNo() + " - Score: " + getCurrentScore() + "/" + getMaxQuestions());
            findViewById(R.id.submit).setVisibility(View.VISIBLE);
            findViewById(R.id.next).setVisibility(View.INVISIBLE);

        }
    };

    /**
     * Getter for the question fragments stored in the activity.
     * @return ArrayList of question fragments.
     */
    public ArrayList<Fragment> getFragments() {
        return fragments;
    }

    /**
     * Getter for current question number
     * @return int currentQuestionNo
     */
    public int getCurrentQuestionNo() {
        return currentQuestionNo;
    }

    /**
     * Setter for current question number
     * @param currentQuestionNo int
     */
    public void setCurrentQuestionNo(int currentQuestionNo) {
        this.currentQuestionNo = currentQuestionNo;
    }

    /**
     * Setter for storing the loaded fragments in the activity.
     * @param fragments ArrayList of question fragments
     */
    public void setFragments(ArrayList<Fragment> fragments) {
        this.fragments = fragments;
    }

    /**
     * Getter for the current score a user is on
     * @return int currentScore
     */
    public int getCurrentScore() {
        return currentScore;
    }

    /**
     * Setter for the current score a user is on
     * @param currentScore int
     */
    public void setCurrentScore(int currentScore) {
        if (currentScore > getMaxQuestions())
            throw new IllegalArgumentException("Score cannot be higher than maxQuestions");
        this.currentScore = currentScore;
    }

    /**
     * Getter for the maximum number of questions in the quiz
     * @return int maxQuestions
     */
    public int getMaxQuestions() {
        return maxQuestions;
    }

    /**
     * Setter for the maximum number of questions in the quiz
     * @param maxQuestions int
     */
    public void setMaxQuestions(int maxQuestions) {
        this.maxQuestions = maxQuestions;
    }
}
