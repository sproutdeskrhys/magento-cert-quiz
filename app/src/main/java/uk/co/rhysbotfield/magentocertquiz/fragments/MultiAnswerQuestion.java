package uk.co.rhysbotfield.magentocertquiz.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import uk.co.rhysbotfield.magentocertquiz.R;

/**
 * Created by rhys4 on 14/10/2017.
 */

public class MultiAnswerQuestion extends Fragment implements Question {

    private String question;
    private String answer;

    /**
     * Inflate fragment layout and set question TextView. Called by Android's Fragment Manager.
     *
     * @return inflated layout.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_multi_answer_question, container, false);
        TextView question = layout.findViewById(R.id.multiquestion);
        question.setText(getQuestion());
        // Inflate the layout for this fragment
        return layout;
    }

    /**
     * Check if the UI has the correct checkboxes selected
     *
     * @return boolean true|false if correct answers are selected
     */
    @Override
    public boolean isCorrect() {
        List<String> answerList = Arrays.asList(getAnswer().split("\\s*,\\s*"));
        CheckBox boxA = getView().findViewById(R.id.checkBoxA);
        CheckBox boxB = getView().findViewById(R.id.checkBoxB);
        CheckBox boxC = getView().findViewById(R.id.checkBoxC);
        CheckBox boxD = getView().findViewById(R.id.checkBoxD);
        // Check if incorrect boxes are selected
        if (boxA.isChecked() && !answerList.contains("A"))
            return false;
        if (boxB.isChecked() && !answerList.contains("B"))
            return false;
        if (boxC.isChecked() && !answerList.contains("C"))
            return false;
        if (boxD.isChecked() && !answerList.contains("D"))
            return false;

        // Make sure correct boxes are selected
        if (!boxA.isChecked() && answerList.contains("A"))
            return false;
        if (!boxB.isChecked() && answerList.contains("B"))
            return false;
        if (!boxC.isChecked() && answerList.contains("C"))
            return false;
        if (!boxD.isChecked() && answerList.contains("D"))
            return false;

        // If checks are passed, return true
        return true;
    }

    /**
     * Check that any answer has been selected.
     * @return true|false if any answer is chosen.
     */
    @Override
    public boolean isSelected() {
        CheckBox boxA = getView().findViewById(R.id.checkBoxA);
        CheckBox boxB = getView().findViewById(R.id.checkBoxB);
        CheckBox boxC = getView().findViewById(R.id.checkBoxC);
        CheckBox boxD = getView().findViewById(R.id.checkBoxD);
        return boxA.isChecked() || boxB.isChecked() || boxC.isChecked() || boxD.isChecked();
    }

    /**
     * Disable clickability on the answers
     */
    @Override
    public void disableAnswers() {
        getView().findViewById(R.id.checkBoxA).setClickable(false);
        getView().findViewById(R.id.checkBoxB).setClickable(false);
        getView().findViewById(R.id.checkBoxC).setClickable(false);
        getView().findViewById(R.id.checkBoxD).setClickable(false);
    }

    /**
     * Display the correct & wrong answers to the user in the UI
     */
    @Override
    public void highlightAnswer() {
        if (getAnswer().contains("A"))
            getView().findViewById(R.id.checkBoxA).setBackgroundResource(R.color.correct);
        else
            getView().findViewById(R.id.checkBoxA).setBackgroundResource(R.color.incorrect);
        if (getAnswer().contains("B"))
            getView().findViewById(R.id.checkBoxB).setBackgroundResource(R.color.correct);
        else
            getView().findViewById(R.id.checkBoxB).setBackgroundResource(R.color.incorrect);
        if (getAnswer().contains("C"))
            getView().findViewById(R.id.checkBoxC).setBackgroundResource(R.color.correct);
        else
            getView().findViewById(R.id.checkBoxC).setBackgroundResource(R.color.incorrect);
        if (getAnswer().contains("D"))
            getView().findViewById(R.id.checkBoxD).setBackgroundResource(R.color.correct);
        else
            getView().findViewById(R.id.checkBoxD).setBackgroundResource(R.color.incorrect);
    }

    /**
     * Setter for private field question
     *
     * @param question String input for question
     */
    @Override
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     * Setter for private field answer
     *
     * @param answer String comma-delimited list of the correct answers. Will only accept values A, B, C, and D
     */
    @Override
    public void setAnswer(String answer) {
        answer = answer.trim();
        List<String> answerList = Arrays.asList(answer.split("\\s*,\\s*"));
        for (int i = 0; i < answerList.size(); i++) {
            if (answerList.get(i).equals("A") || answerList.get(i).equals("B") ||
                    answerList.get(i).equals("C") || answerList.get(i).equals("D"))
                this.answer = answer;
            else
                throw new IllegalArgumentException("answer input is not A, B, C, or D");
        }


    }

    /**
     * Getter for private field question
     *
     * @return String question to display in TextView
     */
    @Override
    public String getQuestion() {
        return question;
    }

    /**
     * Getter for private field answer
     *
     * @return String answer to question
     */
    @Override
    public String getAnswer() {
        return answer;
    }

}
