package uk.co.rhysbotfield.magentocertquiz.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import uk.co.rhysbotfield.magentocertquiz.R;

/**
 * Created by rhys4 on 14/10/2017.
 */

public class SingleAnswerQuestion extends Fragment implements Question {

    private String question;
    private String answer;

    /**
     * Inflate fragment layout and set question TextView. Called by Android's Fragment Manager.
     *
     * @return inflated layout.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_single_answer_question, container, false);
        TextView question = layout.findViewById(R.id.singlequestion);
        question.setText(getQuestion());
        // Inflate the layout for this fragment
        return layout;
    }

    /**
     * Check if the UI has the correct checkboxes selected
     *
     * @return boolean true|false if correct answers are selected
     */
    @Override
    public boolean isCorrect() {
        RadioButton buttonA = getView().findViewById(R.id.singleanswerA);
        RadioButton buttonB = getView().findViewById(R.id.singleanswerB);
        RadioButton buttonC = getView().findViewById(R.id.singleanswerC);
        RadioButton buttonD = getView().findViewById(R.id.singleanswerD);
        if (buttonA.isChecked() && getAnswer().equals("A"))
            return true;
        if (buttonB.isChecked() && getAnswer().equals("B"))
            return true;
        if (buttonC.isChecked() && getAnswer().equals("C"))
            return true;
        if (buttonD.isChecked() && getAnswer().equals("D"))
            return true;
        return false;
    }

    /**
     * Check that any answer has been selected.
     * @return true|false if any answer is chosen.
     */
    @Override
    public boolean isSelected() {
        RadioButton buttonA = getView().findViewById(R.id.singleanswerA);
        RadioButton buttonB = getView().findViewById(R.id.singleanswerB);
        RadioButton buttonC = getView().findViewById(R.id.singleanswerC);
        RadioButton buttonD = getView().findViewById(R.id.singleanswerD);
        return buttonA.isChecked() || buttonB.isChecked() || buttonC.isChecked() || buttonD.isChecked();
    }

    /**
     * Disable clickability on the answers
     */
    @Override
    public void disableAnswers() {
        getView().findViewById(R.id.singleanswerA).setClickable(false);
        getView().findViewById(R.id.singleanswerB).setClickable(false);
        getView().findViewById(R.id.singleanswerC).setClickable(false);
        getView().findViewById(R.id.singleanswerD).setClickable(false);
    }

    /**
     * Display the correct & wrong answers to the user in the UI
     */
    @Override
    public void highlightAnswer() {
        if (getAnswer().equals("A"))
            getView().findViewById(R.id.singleanswerA).setBackgroundResource(R.color.correct);
        else
            getView().findViewById(R.id.singleanswerA).setBackgroundResource(R.color.incorrect);
        if (getAnswer().equals("B"))
            getView().findViewById(R.id.singleanswerB).setBackgroundResource(R.color.correct);
        else
            getView().findViewById(R.id.singleanswerB).setBackgroundResource(R.color.incorrect);
        if (getAnswer().equals("C"))
            getView().findViewById(R.id.singleanswerC).setBackgroundResource(R.color.correct);
        else
            getView().findViewById(R.id.singleanswerC).setBackgroundResource(R.color.incorrect);
        if (getAnswer().equals("D"))
            getView().findViewById(R.id.singleanswerD).setBackgroundResource(R.color.correct);
        else
            getView().findViewById(R.id.singleanswerD).setBackgroundResource(R.color.incorrect);

    }

    @Override
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     * Setter for private field answer
     *
     * @param answer String comma-delimited list of the correct answers. Will only accept values A, B, C, and D
     */
    @Override
    public void setAnswer(String answer) {
        answer = answer.trim();
        if (answer.equals("A") || answer.equals("B") || answer.equals("C") || answer.equals("D"))
            this.answer = answer;
        else
            throw new IllegalArgumentException("answer input is not A, B, C, or D");
    }

    /**
     * Getter for private field question
     *
     * @return String question to display in TextView
     */
    @Override
    public String getQuestion() {
        return question;
    }

    /**
     * Getter for private field answer
     *
     * @return String answer to question
     */
    @Override
    public String getAnswer() {
        return answer;
    }
}
